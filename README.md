Debugging Tutorial
==================

(c) 2021 David Buchmann <mail@davidbu.ch>

A sample application to be used for the debugging tutorial.

Installation
============

Checkout this repository into a (virtual) machine with PHP 7.4 or better and MySQL.

Once the requirements are installed, clone this repository with:

    git clone https://gitlab.com/dbu/wsc21-debugging-tutorial.git

Create a database named `debugging` with username & password `websc`  and then run:

    composer install
    bin/console doctrine:migrations:migrate -n

Usage
=====

This application reads information from CSV (comma separated values) files into a database. It is a
bit overcomplicated for what it does, but the goal is to have a reasonably complex application
where I can hide some bugs...

It consists of an importer that you can run like this:

    bin/console app:import
    
And a web interface that shows you a little bit of information at:

    http://debugging.lo
