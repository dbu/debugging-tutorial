<?php

namespace App\Command;

use App\Importer\LikesImporter;
use App\Importer\ProductsImporter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AppImportCommand extends Command
{
    private EntityManagerInterface $em;
    private ProductsImporter $products;
    private LikesImporter $likes;

    public function __construct(EntityManagerInterface $em, ProductsImporter $products, LikesImporter $likes)
    {
        parent::__construct('app:import');
        $this->em = $em;
        $this->products = $products;
        $this->likes = $likes;
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Import users and products from CSV data')
            ->addOption('reset', null, InputOption::VALUE_NONE, 'Tell the command to first purge the database')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $this->em->beginTransaction();
        if ($input->getOption('reset')) {
            $conn = $this->em->getConnection();
            $conn->executeStatement('DELETE FROM customer_product_likes');
            $conn->executeStatement('DELETE FROM customer');
            $conn->executeStatement('DELETE FROM product');
            $io->success('Emptied the data tables.');
        }

        $this->products->import();
        $this->likes->import();

        $this->em->commit();
        $io->success('Import successfully completed.');

        return 0;
    }
}
