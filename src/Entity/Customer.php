<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CustomerRepository")
 */
class Customer
{
    /**
     * Order ID as provided by the data source.
     *
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    public string $id;

    /**
     * Name of the person placing the order.
     *
     * @ORM\Column(type="string")
     */
    public string $name;

    /**
     * Products that this user likes.
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Product")
     * @ORM\JoinTable(name="customer_product_likes")
     * @var Collection<int, Product>
     */
    public Collection $likedProducts;

    public function __construct()
    {
        $this->likedProducts = new ArrayCollection();
    }

    public static function createFromData(string $id, string $name): Customer
    {
        $like = new self();
        $like->id = $id;
        $like->name = $name;

        return $like;
    }
}
