<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * Product GUID provided by the source system.
     *
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    public string $id;

    /**
     * The product name.
     *
     * @ORM\Column(type="string")
     */
    public string $name;

    /**
     * Price in cents.
     *
     * @ORM\Column(type="integer")
     */
    public int $price;

    public static function createFromData(string $id, string $name, int $price): Product
    {
        $product = new self();
        $product->id = $id;
        $product->name = $name;
        $product->price = $price;

        return $product;
    }
}
