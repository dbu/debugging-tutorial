<?php
namespace App\Importer;


use App\Importer\Loader\LikeLoader;
use App\Importer\Mapper\CustomerMapper;
use App\Importer\Mapper\LikeMapper;
use Doctrine\ORM\EntityManagerInterface;

class LikesImporter
{
    private EntityManagerInterface $em;
    private LikeLoader $loader;
    private CustomerMapper $customerMapper;
    private LikeMapper $likeMapper;

    public function __construct(
        EntityManagerInterface $em,
        LikeLoader $loader,
        CustomerMapper $customerMapper,
        LikeMapper $likeMapper
    ) {
        $this->loader = $loader;
        $this->likeMapper = $likeMapper;
        $this->em = $em;
        $this->customerMapper = $customerMapper;
    }

    public function import(): void
    {
        foreach ($this->loader->loadLikes() as $likeData) {
            $customer = $this->customerMapper->map($likeData);
            $this->em->persist($customer);

            $this->likeMapper->map($customer, $likeData);
        }

        $this->em->flush();
    }
}
