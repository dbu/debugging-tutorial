<?php

namespace App\Importer\Loader;

use Psr\Log\LoggerInterface;
use RuntimeException;
use Safe\Exceptions\FilesystemException;
use function Safe\fopen;
use function Safe\fclose;

class CsvProductLoader extends CsvLoader implements ProductLoader
{
    /**
     * @var string Absolute path to the csv product file
     */
    private string $file;

    /**
     * @param string          $csvFile Absolute path to the csv product file
     * @param LoggerInterface $logger
     */
    public function __construct(string $csvFile, LoggerInterface $logger)
    {
        parent::__construct($logger);
        $this->file = $csvFile;
    }

    public function loadProducts(): iterable
    {
        try {
            $handle = fopen($this->file, 'rb');
        } catch(FilesystemException $e) {
            throw new RuntimeException('Could not open CSV file '.$this->file, 0, $e);
        }

        $products = $this->readCsvFile($handle, ['Id', 'Name', 'Price']);
        fclose($handle);

        return $products;
    }
}
