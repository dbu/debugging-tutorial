<?php

namespace App\Importer\Loader;

interface LikeLoader
{
    /**
     * @return array<array<string, string>>
     */
    public function loadLikes(): iterable;
}
