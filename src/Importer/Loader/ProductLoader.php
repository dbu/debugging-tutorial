<?php

namespace App\Importer\Loader;

interface ProductLoader
{
    /**
     * @return array<array<string, string>>
     */
    public function loadProducts(): iterable;
}
