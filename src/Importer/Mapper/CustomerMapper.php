<?php

namespace App\Importer\Mapper;

use App\Entity\Customer;
use App\Repository\CustomerRepository;

class CustomerMapper
{
    /**
     * Lookup cache for newly created customers.
     *
     * We expect to be given the same customer several times during import,
     * once for each row in the likes CSV. The doctrine repository will only
     * `find` new entities after flushing, however.
     *
     * This naive approach will only work if the data set does not become too
     * big. If there is a large number of customers, this will fill up the
     * memory and eventually crash the importer. A more robust approach would
     * periodically flush Doctrine after some number of rows and then reset
     * this cache as well as reset Doctrine itself to reset its cache of
     * managed entities.
     *
     * @var Customer[] Hashmap of customer id => customer entity
     */
    private array $customers = [];
    private CustomerRepository $customerRepository;

    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param array<string, string> $data
     */
    public function map(array $data): Customer
    {
        if (array_key_exists($data['CustomerId'], $this->customers)) {
            $customer = $this->customers[$data['CustomerId']];
            $customer->name = $data['Name'];

            return $customer;
        }

        $customer = $this->customerRepository->find($data['CustomerId']);
        if ($customer) {
            $customer->name = $data['Name'];

            return $customer;
        }

        $customer = Customer::createFromData($data['CustomerId'], $data['Name']);
        $this->customers[$customer->id] = $customer;

        return $customer;
    }
}
