<?php

namespace App\Importer\Mapper;

use App\Entity\Product;
use App\Repository\ProductRepository;

class ProductMapper
{
    private ProductRepository $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @param array<string, string> $data
     */
    public function map(array $data): Product
    {
        $product = $this->productRepository->find($data['Id']);
        if ($product) {
            $product->name = $data['Name'];
            $product->price = (int) $data['Price'];

            return $product;
        }

        return Product::createFromData($data['Id'], $data['Name'], (int) $data['Price']);
    }
}
