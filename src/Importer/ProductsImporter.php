<?php
namespace App\Importer;


use App\Importer\Loader\ProductLoader;
use App\Importer\Mapper\ProductMapper;
use Doctrine\ORM\EntityManagerInterface;

class ProductsImporter
{
    private EntityManagerInterface $em;
    private ProductLoader $loader;
    private ProductMapper $mapper;

    public function __construct(EntityManagerInterface $em, ProductLoader $loader, ProductMapper $mapper)
    {
        $this->loader = $loader;
        $this->mapper = $mapper;
        $this->em = $em;
    }

    public function import(): void
    {
        foreach ($this->loader->loadProducts() as $productData) {
            $product = $this->mapper->map($productData);
            $this->em->persist($product);
        }

        $this->em->flush();
    }
}
