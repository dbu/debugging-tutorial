<?php

namespace App\Tests\Functional\Command;

use App\Command\AppImportCommand;
use App\Tests\Functional\DatabasePurger;
use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class AppImportCommandTest extends KernelTestCase
{
    private AppImportCommand $command;

    private Connection $connection;

    public function setUp(): void
    {
        $kernel = static::createKernel();
        $kernel->boot();
        $application = new Application($kernel);
        $this->command = $application->find('app:import');

        $this->connection = $kernel->getContainer()->get('database_connection');
        DatabasePurger::purge($this->connection);
    }

    public function testRun(): void
    {
        $commandTester = new CommandTester($this->command);
        $commandTester->execute([
            $this->command->getName(),
            '--reset' => true,
        ]);

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Import successfully completed', $output);

        $this->assertSame('2', $this->connection->fetchOne('SELECT COUNT(*) FROM customer'));
        $this->assertSame('2', $this->connection->fetchOne('SELECT COUNT(*) FROM product'));
        $this->assertSame('3', $this->connection->fetchOne('SELECT COUNT(*) FROM customer_product_likes'));
    }
}
