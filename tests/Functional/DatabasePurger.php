<?php

namespace App\Tests\Functional;

use Doctrine\DBAL\Connection;

class DatabasePurger
{
    public static function purge(Connection $conn): void
    {
        $conn->executeStatement('DELETE FROM customer_product_likes');
        $conn->executeStatement('DELETE FROM customer');
        $conn->executeStatement('DELETE FROM product');
    }
}
