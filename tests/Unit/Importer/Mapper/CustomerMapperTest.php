<?php

namespace App\Tests\Unit\Importer\Mapper;

use App\Entity\Customer;
use App\Entity\Product;
use App\Importer\Mapper\CustomerMapper;
use App\Repository\CustomerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CustomerMapperTest extends TestCase
{
    /**
     * @var CustomerRepository&MockObject
     */
    private CustomerRepository $repository;

    protected function setUp(): void
    {
        $this->repository = $this->createMock(CustomerRepository::class);
    }

    public function testCreateCustomer(): void
    {
        $this->repository
            ->expects($this->once())
            ->method('find')
            ->with('1607080270099')
            ->willReturn(null)
        ;

        $mapper = new CustomerMapper($this->repository);
        $customer = $mapper->map([
            'CustomerId' => '1607080270099',
            'Name' => 'Quyn Goff',
            'ProductId' => 'CA06E2F2-AFB3-D0CB-2B96-63FD98E9C7AC',
        ]);

        $this->assertSame('1607080270099', $customer->id);
        $this->assertSame('Quyn Goff', $customer->name);
        $this->assertCount(0, $customer->likedProducts);

        // test built-in cache
        $updatedCustomer = $mapper->map([
            'CustomerId' => '1607080270099',
            'Name' => 'New Name',
            'ProductId' => 'CA06E2F2-AFB3-D0CB-2B96-63FD98E9C7AC',
        ]);

        $this->assertSame($customer, $updatedCustomer);
        $this->assertSame('New Name', $customer->name);
    }

    public function testUpdateCustomer(): void
    {
        $existingCustomer = new Customer();
        $existingCustomer->id = '1607080270099';
        $existingCustomer->name = 'Old Name';
        $existingCustomer->likedProducts = new ArrayCollection();
        $existingCustomer->likedProducts->add(new Product());

        $this->repository
            ->expects($this->once())
            ->method('find')
            ->with('1607080270099')
            ->willReturn($existingCustomer)
        ;

        $mapper = new CustomerMapper($this->repository);
        $customer = $mapper->map([
            'CustomerId' => '1607080270099',
            'Name' => 'Quyn Goff',
            'ProductId' => 'CA06E2F2-AFB3-D0CB-2B96-63FD98E9C7AC',
        ]);

        $this->assertSame($existingCustomer, $customer);
        $this->assertSame('Quyn Goff', $customer->name);
        $this->assertCount(1, $customer->likedProducts);
    }
}
