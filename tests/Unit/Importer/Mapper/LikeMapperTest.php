<?php

namespace App\Tests\Unit\Importer\Mapper;

use App\Entity\Customer;
use App\Entity\Product;
use App\Importer\Mapper\LikeMapper;
use App\Repository\ProductRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class LikeMapperTest extends TestCase
{
    /**
     * @var ProductRepository&MockObject
     */
    private ProductRepository $repository;

    protected function setUp(): void
    {
        $this->repository = $this->createMock(ProductRepository::class);
    }

    public function testAddLike(): void
    {
        $product = new Product();
        $product->id = 'CA06E2F2-AFB3-D0CB-2B96-63FD98E9C7AC';
        $this->repository
            ->expects($this->once())
            ->method('find')
            ->with('CA06E2F2-AFB3-D0CB-2B96-63FD98E9C7AC')
            ->willReturn($product);

        $mapper = new LikeMapper($this->repository);
        $customer = new Customer();
        $mapper->map($customer, [
            'CustomerId' => '1607080270099',
            'Name' => 'Quyn Goff',
            'ProductId' => 'CA06E2F2-AFB3-D0CB-2B96-63FD98E9C7AC',
        ]);

        $this->assertCount(1, $customer->likedProducts);
        $likedProduct = $customer->likedProducts->first();
        $this->assertSame($product, $likedProduct);
    }

    public function testNoDuplicatedLike(): void
    {
        $product = new Product();
        $product->id = 'CA06E2F2-AFB3-D0CB-2B96-63FD98E9C7AC';
        $this->repository
            ->expects($this->once())
            ->method('find')
            ->with('CA06E2F2-AFB3-D0CB-2B96-63FD98E9C7AC')
            ->willReturn($product)
        ;

        $mapper = new LikeMapper($this->repository);
        $customer = new Customer();
        $customer->likedProducts->add($product);
        $mapper->map($customer, [
            'CustomerId' => '1607080270099',
            'Name' => 'Quyn Goff',
            'ProductId' => 'CA06E2F2-AFB3-D0CB-2B96-63FD98E9C7AC',
        ]);

        $this->assertCount(1, $customer->likedProducts);
        $likedProduct = $customer->likedProducts->first();
        $this->assertSame($product, $likedProduct);
    }

    public function testProductNotFound(): void
    {
        $this->repository
            ->expects($this->once())
            ->method('find')
            ->with('CA06E2F2-AFB3-D0CB-2B96-63FD98E9C7AC')
            ->willReturn(null)
        ;

        $mapper = new LikeMapper($this->repository);
        $customer = new Customer();

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('not found');
        $mapper->map($customer, [
            'CustomerId' => '1607080270099',
            'Name' => 'Quyn Goff',
            'ProductId' => 'CA06E2F2-AFB3-D0CB-2B96-63FD98E9C7AC',
        ]);
    }
}
